import asyncio
import sys
import fitz
from extractor import extract_item_rects, create_zip, create_docx

if len(sys.argv) != 3:
    print(f'Usage: {sys.argv[0]} annotated-pdf output.pdf|output.docx')
    sys.exit(1)
pdf = sys.argv[1]
out = sys.argv[2]
with open(pdf, 'rb') as f:
    pdfdata = f.read()
pdfdoc = fitz.open(stream=pdfdata, filetype="pdf")
items = extract_item_rects(pdfdoc)
if not items:
    print('No regions defined in the PDF file')
    sys.exit(2)
if out.endswith('.zip'):
    asyncio.run(create_zip(pdfdoc, items, out))
elif out.endswith('.docx'):
    asyncio.run(create_docx(pdfdoc, items, out))
else:
    print('Unsupported output extension')

