import sys
from io import BytesIO
import asyncio
import itertools
import aiofiles
from zipstream import AioZipStream
import fitz
from docx import Document
from docx.shared import Mm, Pt
import config


def cluster(rects):
    '''
    Cluster a list of rectagles from the same page into columns based on their
    left borders.  Return a list of clusters.
    '''
    clusters = []
    for r in rects:
        if not clusters:
            clusters.append([r.x0, [r]])
        else:
            best_matched = min(clusters, key=lambda x: abs(r.x0-x[0]))
            if abs(best_matched[0] - r.x0) > config.CLUSTER_MAX_DIST:
                clusters.append([r.x0, [r]])
            else:
                best_matched[1].append(r)
                # recompute cluster's centroid
                best_matched[0] = sum(r.x0 for r in best_matched[1])/len(best_matched[1])

    return clusters


def group_items(cluster):
    '''
    Group rectagles in a cluster based on how they contain one another.
    Return a list of groups.

    Notes: this function will destroy the order of rectangles in the cluster.
    '''
    groups = []  # list of (<task>,[<choice>...])

    # go through the list by rects' y-coordinates
    cluster.sort(key=lambda r: r.y0)
    for rect in cluster:
        if not groups or not groups[-1]['parent'].contains(rect.top_left):
            groups.append({
                'parent': rect,
                'choices': [],
            })
        else:
            groups[-1]['choices'].append(rect)
    return groups


def extract_item_rects(pdfdoc):
    '''
    Extract regions annotated by rectangles from the specified PDF stream.
    Questions in each page are sorted in a column-major order.
    '''
    items = []
    for i, page in enumerate(pdfdoc):
        rects = [ann.rect for ann in page.annots() if ann.type[1] == 'Square']
        clusters = cluster(rects)
        clusters.sort(key=lambda c:c[0])
        for x, c in clusters:
            for item in group_items(c):
                items.append({
                    'page': i,
                    'item': item,
                })

    return items


def generate_contents(doc, items):
    async def make_stream(data):
        yield data

    for page_idx, items_iter in itertools.groupby(items, lambda x:x['page']):
        page = doc[page_idx]
        for i, item_info in enumerate(items_iter):
            folder_name = f'page{page_idx+1:02d}-{i+1:02d}'
            task_name = f'{folder_name}/_task.png'
            item = item_info['item']
            parent = item['parent']
            choices = item['choices']
            if choices:
                # clip the task region up to the top of the first choice
                parent.y1 = choices[0].y0
            pix = page.get_pixmap(dpi=config.DPI, annots=False, clip=parent)
            yield {
                'stream': make_stream(pix.tobytes(output='png')),
                'name': task_name,
                'page': page_idx,
                'item': i,
            }
            for j, choice in enumerate(choices):
                choice_name = f'{folder_name}/choice-{j+1}.png'
                pix = page.get_pixmap(dpi=config.DPI, annots=False, clip=choice)
                yield {
                    'stream': make_stream(pix.tobytes(output='png')),
                    'name': choice_name,
                }


def create_aio_zip_stream(pdfdoc, items):
    return AioZipStream(generate_contents(pdfdoc, items), chunksize=32768)


async def create_zip(pdfdoc, items, out):
    data = create_aio_zip_stream(pdfdoc, items)
    async with aiofiles.open(out, 'wb') as zipfile:
        async for chunk in data.stream():
            await zipfile.write(chunk)


async def create_aio_docx_stream(pdfdoc, items):
    document = Document()
    section = document.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)
    paragraph_format = document.styles['Normal'].paragraph_format
    paragraph_format.space_before = Mm(0)
    paragraph_format.space_after = Mm(0)

    for part in generate_contents(pdfdoc, items):
        name = part['name']
        if name.endswith('_task.png'):
            p = document.add_paragraph('')
            page = part['page'] + 1
            item = part['item'] + 1
            p.add_run(f'Page {page} / No. {item}').bold = True
            p.paragraph_format.space_before = Pt(12)
        async for pic in part['stream']:
            buf = BytesIO(pic)
            document.add_picture(buf)

    out = BytesIO()
    document.save(out)
    out.seek(0)

    yield out.getvalue()


async def create_docx(pdfdoc, items, out):
    data = create_aio_docx_stream(pdfdoc, items)
    async with aiofiles.open(out, 'wb') as docxfile:
        async for chunk in data:
            await docxfile.write(chunk)
