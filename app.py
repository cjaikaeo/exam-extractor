import logging
import fitz
from extractor import (
    extract_item_rects, create_aio_zip_stream, create_aio_docx_stream
)
from quart import (
    Quart, Response, render_template, url_for, request, jsonify, make_response
)
import config

logging.basicConfig(
        filename='server.log',
        format='[%(asctime)s.%(msecs)03d] %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO)
app = Quart(__name__)
app.config['MAX_CONTENT_LENGTH'] = config.MAX_PDF_SIZE
PREFIX = config.APP_ROOT


@app.route(f"{PREFIX}/")
async def hello():
    return await render_template('index.html')


async def extract_to_zip(pdfdoc, items):
    data = create_aio_zip_stream(pdfdoc, items)
    filename = config.OUT_FILE_NAME + '.zip'
    return Response(
        data.stream(),
        mimetype='application/zip',
        headers={'Content-Disposition': f'attachment;filename={filename}'},
    )


async def extract_to_docx(pdfdoc, items):
    data = create_aio_docx_stream(pdfdoc, items)
    filename = config.OUT_FILE_NAME + '.docx'
    return Response(
        data,
        mimetype='application/docx',
        headers={'Content-Disposition': f'attachment;filename={filename}'},
    )


@app.route(f"{PREFIX}/extract", methods=['POST'])
async def extract():
    files = await request.files
    file = files['file']
    form = await request.form
    output = form['output']
    try:
        pdfdoc = fitz.open(stream=file.read(), filetype="pdf")
        items = extract_item_rects(pdfdoc)
        if not items:
            raise Exception('no regions defined in the PDF file')
        if output == 'zip':
            return await extract_to_zip(pdfdoc, items)
        elif output == 'docx':
            return await extract_to_docx(pdfdoc, items)
        else:
            raise Exception('Unexpected error')
    except Exception as e:
        return await render_template('error.html', message=str(e))


###############################
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5555)
