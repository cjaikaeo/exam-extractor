# Exam Extractor

A Quart-based web application for extracting exam regions from a PDF file into
images and returning them as a .zip or .docx file.  Regions are defined by creating
rectangular annotations inside the PDF file.
